<?php
/**
 * This function will register CSS & JS using wp_head() in header.php and wp_footer() in footer.php
 *
 * Function is triggered by
 * ```
 * add_action('wp_enqueue_scripts', 'script_enqueue')
 * ```
 */
function script_enqueue()
{
	// Register CSS files
	wp_enqueue_style( 'BootstrapV4', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css', [], '4.0.0', 'all' );
	wp_enqueue_style( 'CustomUniqueNameHere', get_template_directory_uri() .  '/css/main.css', [], filemtime(get_stylesheet_directory() . '/css/main.css'), 'all' );


	// Register JS files
	wp_enqueue_script( 'jQuery', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', [], null, true );
	wp_enqueue_script( 'Popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', [], '4.0.0', true );
	wp_enqueue_script( 'Bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js', [], '4.0.0', true );
	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/js/main.js', [], filemtime(get_stylesheet_directory() . '/js/main.js'), true );
}

add_action( 'wp_enqueue_scripts', 'script_enqueue' );

/**
 * This function creates new page in left menu inside WP Admin called 'Options'
 * You'll need to have ACF Pro active in order for this function to work.
 *
 * Advanced Custom Fields that are used inside this page are commonly used
 * in header (logo image), footer (contact informations like email, phone num, etc.)
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page([
		'page_title' => 'General',
		'menu_title' => 'General'
	]);
}