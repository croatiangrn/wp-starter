<?php
// `logo` field is Image type and is set to return an array (id, url, width, height, alt, etc.)
// If the image is not set, $logo will be null
$logo = get_field( 'logo', 'options' );
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
            </div>
        </div>
    </div>

<?php
// Check for a repeater field if it's not empty
if ( have_rows( 'moji_tekstovi' ) ) {
	?>
    <div class="container">
		<?php
		while ( have_rows( 'moji_tekstovi' ) ) {
			the_row();

			$title   = get_sub_field( 'naslov' );
			$content = get_sub_field( 'sadrzaj_teksta' );
			?>
            <div class="row">
                <div class="col-md-6"><h1><?= $title ?></h1></div>
                <div class="col-md-6"><?= $content ?></div>
            </div>
			<?php
		}
		?>
    </div>
	<?php
}