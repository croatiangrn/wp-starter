</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?= get_field( 'my_email', 'options' ) ?>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>